#include "game_of_life.h"
#include "area.h"

void free_area(u_char **area) {
    if (area) {
        uint32_t i = 0;
        while (area[i]) {
            free(area[i++]);
        }
        free(area);
    }
}

u_char **create_area(uint32_t height, uint32_t width) {
    u_char **area = (u_char**)malloc(sizeof(char*) * (height + 1));
    
    if (area == NULL) {
        return NULL;
    }

    uint32_t i = 0;
    while (i < height) {
        area[i] = (u_char*)malloc(sizeof(u_char) * (width + 1));
        if (area[i] == NULL) {
            free_area(area);
            return NULL;
        }
        memset(area[i], CHAR_EMPTY, sizeof(u_char) * width);
        area[i][width] = 0;
        i++;
    }
    area[i] = 0;
    return area;
}

void display_area(const t_gol *gol) {
    uint32_t i = 0;

    printf("|");
    while (i < gol->width) {
        printf("--");
        i++;
    }
    i = 0;
    while (gol->area[i]) {
        uint32_t j = 0;
        printf("|\n|");
        while (gol->area[i][j]) {
            if (gol->area[i][j] == CHAR_LIVE) {
                uint8_t nb = get_neighboors_live(gol, i, j);

                printf("\x1b[48;5;%dm\x1b[30m  \x1b[0m", nb + 75);
            } else {
                printf("  ");
            }
            j++;
        }
        i++;
    }
    i = 0;
    printf("|\n|");
    while (i < gol->width) {
        printf("--");
        i++;
    }
    printf("|\n");
    i = 0;
}

uint8_t neighbors_live(const t_gol *gol, int32_t height, int32_t width) {
    if (width < 0) {
        if (!(gol->opt.fl & enum_PORTAL)) return 0;
        width = gol->width - 1;
    } else if (width >=  (int32_t)gol->width) {
        if (!(gol->opt.fl & enum_PORTAL)) return 0;
        width = 0;
    }
    
    if (height < 0) {
        if (!(gol->opt.fl & enum_PORTAL)) return 0;
        height = gol->height - 1;
    }else if (height >= (int32_t)gol->height) {
        if (!(gol->opt.fl & enum_PORTAL)) return 0;
        height = 0;
    }
    return gol->area[height][width] == CHAR_LIVE;
}

uint8_t get_neighboors_live(const t_gol *gol, uint32_t i, uint32_t j) {
    uint8_t neighbors = 0;
    //top
    neighbors += neighbors_live(gol, i - 1, j - 1);
    neighbors += neighbors_live(gol, i - 1, j);
    neighbors += neighbors_live(gol, i - 1, j + 1);
    //center
    neighbors += neighbors_live(gol, i, j - 1);
    neighbors += neighbors_live(gol, i, j + 1);
    // bottom
    neighbors += neighbors_live(gol, i + 1, j - 1);
    neighbors += neighbors_live(gol, i + 1, j);
    neighbors += neighbors_live(gol, i + 1, j + 1);
    return neighbors;
}

void live_area(t_gol *gol) {
    u_char **tmp  = create_area(gol->height, gol->width);
    if (tmp == NULL) {
        errx(EXIT_FAILURE, "error alloc area in `live_area`");
    }

    uint32_t i = 0;
    gol->nb_live = 0;


    while (gol->area[i]) {
        uint32_t j = 0;
        while (gol->area[i][j]) {
            uint8_t neighbors = get_neighboors_live(gol, i, j);
            // cell is live
            if (neighbors == 3 || (gol->area[i][j] == CHAR_LIVE && neighbors == 2)) {
                tmp[i][j] = CHAR_LIVE;
                gol->nb_live++;
            }
            j++;
        }
        i++;
    }

    free_area(gol->area);
    gol->area = tmp;
}