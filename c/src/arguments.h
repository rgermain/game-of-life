#ifndef __ARGUMENTS_H__
# define __ARGUMENTS_H__
# include <argp.h>
# include <stdint.h>

enum e_flags {
    enum_FILE = 0b1,
    enum_STEP = 0b10,
    enum_PORTAL = 0b100
};

typedef struct s_opt {
    uint32_t fl;
    const char *file;
} t_opt;

error_t argument_parse(t_opt *opt, int ac, char **av);

#endif