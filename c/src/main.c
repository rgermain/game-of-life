#include "game_of_life.h"
#include "arguments.h"
#include "area.h"
#include <fcntl.h>
#include <sys/ioctl.h>

static void init_gol(t_gol *gol) {

    struct winsize w;
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);

    if (w.ws_row != 0 && w.ws_col != 0) {
        gol->width = w.ws_col / 2 - 1;
        gol->height = w.ws_row - 6;
        printf("size from terminal is: %dx%d\n", gol->width, gol->height);
    } else {
        gol->height = 60;
        gol->width = 30;
        printf("c'ant get terminal size, default is: %dx%d\n", gol->width, gol->height);
    }

    gol->area = create_area(gol->height, gol->width);
    if (gol->area == NULL) {
        errx(EXIT_FAILURE, "error alloc area in `main`");
    }

    if (gol->opt.fl & enum_FILE) {
        int32_t fd = open(gol->opt.file, O_RDONLY);
        if (fd < 0) {
            free_area(gol->area);
            errx(EXIT_FAILURE, "can't open file `%s`, exit...\n", gol->opt.file);
        }

        uint32_t width = 0;
        uint32_t height = 0;

        char buff[50];
        while (true) {
            int32_t size = read(fd, buff, 50);
            if (size == 0) {
                break ;
            } else if (size == -1) {
                free_area(gol->area);
                errx(EXIT_FAILURE, "can't read file\n");
            }
            int32_t i = 0;
            while (i < size) {
                if (buff[i] == '\n') {
                    height++;
                    width = 0;
                } else if (buff[i] != CHAR_LIVE) {
                    width++;
                } else if (width < gol->width && height < gol->height) {
                    gol->area[height][width++] = CHAR_LIVE;
                }
                i++;
            }
        }
        close(fd);
    }
}

int main(int ac, char **av) {

    t_gol gol;

    memset(&gol, 0, sizeof(gol));
    if (argument_parse(&(gol.opt), ac, av)) {
        return EXIT_FAILURE;
    }

    init_gol(&gol);

    uint64_t cycle = 0;
    do {
        display_area(&gol);
        if (gol.opt.fl & enum_STEP) {
            getchar();
        } else {
            usleep(40000);
        }
        live_area(&gol);
        printf("cell lives %ld\ncycle: %ld\n", gol.nb_live, cycle++);
    }
    while (gol.nb_live > 0);

    free_area(gol.area);
    return EXIT_SUCCESS;
}
