#include <argp.h>
#include "arguments.h"

const char *argp_program_version = "0.1.0";
const char *argp_program_bug_address = "https://github.com/remigermain";

static error_t parse_flags(int key, __attribute__((unused)) char *arg, struct argp_state *state) {
    t_opt *opt = state->input;

    switch (key) {
        case 's':
            opt->fl |= enum_STEP;
            break ;
        case 'p':
            opt->fl |= enum_PORTAL;
            break ;
        case 'f':
            opt->fl |= enum_FILE;
            opt->file = arg;
            break ;
        default:
            return ARGP_ERR_UNKNOWN;
    }
    return 0;
}

error_t argument_parse(t_opt *opt, int ac, char **av) {

    const struct argp_option options[] = {
        { "step", 's', 0, 0, "enable step.", 0 },
        { "portal", 'p', 0, 0, "all edge is connected (top <--> bottom / left <--> right).", 0 },
        { "file", 'f', "file", 0, "import game of life file.", 0 },
        {0}
    };

    const struct argp arguments = {
        // options
        options,
        // parser
        parse_flags,
        // args_doc
        "<file>",
        // doc
        0,
        // argp_child
        0,
        // help_filter
        0,
        // argp_domain
        0
    };

    return argp_parse(&arguments, ac, av, 0, 0, opt);
}