#ifndef __MAPS_H__
# define __MAPS_H__
# include "game_of_life.h"

#define CHAR_LIVE 'X'
#define CHAR_EMPTY '.'

u_char **create_area(uint32_t with, uint32_t height);
void display_area(const t_gol *gol);
void live_area(t_gol *gol);
void free_area(u_char **area);
uint8_t get_neighboors_live(const t_gol *gol, uint32_t i, uint32_t j);

#endif