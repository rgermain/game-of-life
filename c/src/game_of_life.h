#ifndef __GAME_OF_LIFE_H__
# define __GAME_OF_LIFE_H__
# include <stdint.h>
# include <stdlib.h>
# include <stdio.h>
# include <unistd.h>
# include <string.h>
# include <stdbool.h>
# include <err.h>
# include "arguments.h"

typedef struct s_gol {
    t_opt opt;
    uint32_t width;
    uint32_t height;
    u_char **area;
    uint64_t nb_live;
} t_gol;

#endif