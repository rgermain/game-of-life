use std::convert::TryFrom;

pub struct Opt {
    pub portal: bool,
    pub file: String,
    pub step: bool
}

pub struct Gol {
    pub area: Vec<Vec<u8>>,
    pub width: i32,
    pub height: i32,
    pub opt: Opt,
}

impl Gol {
    fn calc_neighboors(&self, height: i32 , width: i32) -> u8 {
    
        let mut w = width;
        let mut h = height;
        
        // width sanitize
        if width < 0 {
            if !self.opt.portal {
                return 0
            }
            w = self.width -1;
        } else if width >= self.width {
            if !self.opt.portal {
                return 0;
            }
            w = 0;
        }
        // height sanitiz
        if height < 0 {
            if !self.opt.portal {
                return 0
            }
            h = self.height -1;
        } else if height >= self.height {
            if !self.opt.portal {
                return 0;
            }
            h = 0;
        }

        let h = match usize::try_from(h) { Ok(nb) => nb, Err(e) => panic!("{}", e) };
        let w = match usize::try_from(w) { Ok(nb) => nb, Err(e) => panic!("{}", e) };
        return if self.area[h][w] == 0 { 0 } else { 1 }
    }

    fn neighboors_lives(&self, h: usize, w: usize) -> u8 {
        let mut neighboors = 0;
        let w = match i32::try_from(w) { Ok(nb) => nb, Err(e) => panic!("{}", e) };
        let h = match i32::try_from(h) { Ok(nb) => nb, Err(e) => panic!("{}", e) };
        // top
        neighboors += self.calc_neighboors(h - 1, w - 1);
        neighboors += self.calc_neighboors(h - 1, w);
        neighboors += self.calc_neighboors(h - 1, w + 1);
        // center
        neighboors += self.calc_neighboors(h, w - 1);
        neighboors += self.calc_neighboors(h, w + 1);
        // bottom
        neighboors += self.calc_neighboors(h + 1, w - 1);
        neighboors += self.calc_neighboors(h + 1, w);
        neighboors += self.calc_neighboors(h + 1, w + 1);
    
        neighboors
    }



    pub fn lives_area(& mut self) -> usize {
        
        let mut lives: usize = 0;
        let mut next_area = Gol::create_area(self.height, self.width);

        for (h, line) in self.area.iter().enumerate() {
            for (w, cell) in line.iter().enumerate() {
                let neighboors = self.neighboors_lives(h, w);
                if neighboors == 3 || (neighboors == 2 && *cell != 0) {
                    next_area[h][w] = neighboors;
                    lives += 1;
                }
            }
        }
    
        // replace with new area
        self.area = next_area;
    
        lives
    }

    /// display area
    pub fn display_area(&self) {

        let width = self.area[0].len();

        // start line
        print!("|");
        for _ in 0..(width*2) {
            print!("-");
        }

        for (h, line) in self.area.iter().enumerate() {
            print!("|\n|");
            for (w, cell) in line.iter().enumerate() {
                if *cell != 0 {
                    let neighboors = self.neighboors_lives(h, w);
                    print!("\x1b[48;5;{}m\x1b[30m {}\x1b[0m", neighboors + 75, neighboors);
                } else {
                    print!("  ");
                }
            }
        }
        // end line
        print!("|\n|");
        for _ in 0..(width*2) {
            print!("-");
        }
        println!("|");
    }

    pub fn create_area(height: i32, width: i32) -> Vec<Vec<u8>> {
        let mut area = Vec::new();
    
        for _ in 0..height {
            let mut sub = Vec::new();
            for _ in 0..width {
                sub.push(0);
            }
            area.push(sub);
        }
        
        area
    }
}
