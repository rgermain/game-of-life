mod game_of_life;

use game_of_life::{Gol, Opt};

use std::thread::sleep;
use std::time::Duration;

use std::fs::File;
use std::io::BufReader;
use std::io::prelude::*;
use std::io::stdin;

extern crate argparse;

use argparse::{ArgumentParser, StoreTrue, Store};

use terminal_size::{Width, Height, terminal_size};

const CELL_LIVE: char = 'X';

fn parse_opt(gol: &mut Gol) {
    let mut ap = ArgumentParser::new();
        ap.set_description("Game of life game.");

        ap.refer(&mut gol.opt.step)
        .add_option(&["-s", "--step"], StoreTrue, "enable step mode, press <enter> key to next cycle.");

        ap.refer(&mut gol.opt.portal)
            .add_option(&["-p", "--portal"], StoreTrue, "all edge is connected (top <--> bottom / left <--> right).");
        
        ap.refer(&mut gol.opt.file)
            .add_option(&["-f", "--file"], Store, "import game of life file.");
        
        ap.parse_args_or_exit();
}


fn file_replace(area: &mut Vec<Vec<u8>>, file_name: &String) {
    
    let file = File::open(file_name).expect("c'ant open file");
    let reader = BufReader::new(file);

    let height = area.len();
    let width = area[0].len();

    for (h, line) in reader.lines().enumerate() {
        let line = line.expect("error");
        
        if h < height {
            for (w, value) in line.chars().enumerate() {
                if value == CELL_LIVE && w < width {
                    area[h][w] = 1;
                }
            }
        }

    }
    
}

fn main() {
    // get terminal size
    let size = terminal_size();

    // default size
    let mut width = 60;
    let mut height = 30;
    
    if let Some((Width(conv_w), Height(conv_h))) = size {
        width = i32::try_from(conv_w).expect("err") / 2 - 1;
        height = i32::try_from(conv_h).expect("err") - 6;
        println!("size from terminal is {} {}", width, height);
    } else {
        println!("c'ant get terminal size, default is: {}x{}", width, height);
    }

    let mut gol = Gol {
        area: Gol::create_area(height, width),
        width: width,
        height: height,
        opt: Opt {
            portal: false,
            file: String::new(),
            step: false
        }
    };

    parse_opt(&mut gol);
    if gol.opt.file.len() != 0 {
        file_replace(&mut gol.area, &gol.opt.file);
    }


    let mut cycles = 0;
    loop {
        gol.display_area();

        if gol.opt.step {
            let mut s = String::new();
            stdin().read_line(&mut s).expect("err");
        } else {
            sleep(Duration::from_millis(50));
        }

        let lives = gol.lives_area();
        println!("cell lives {}\ncycle: {}\n", lives, cycles);
        cycles += 1;
        if lives == 0 {
            break ;
        }
    }

}
