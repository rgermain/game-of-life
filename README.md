# game-of-life

The Game of Life, also known simply as Life, is a board game originally created in 1860 by Milton Bradley, as The Checkered Game of Life.

## Getting started

### C
```sh
make
./game-of-line [options..]
```
exemple:
```sh
./game-of-line --file ../cells/big -p -s
```

### Rust
```sh
cargo build -- [options...]
```
exemple:
```sh
cargo build -- --file ../cells/big -p -s
```

## Options
`-s`, `--step`  step mode, press `enter`  to next cycles

`-p`, `--portal` all edge is connected (top <--> bottom / left <--> right).

`-f`, `--file` import game of life file.
